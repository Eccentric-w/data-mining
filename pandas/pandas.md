# pandas

行索引：index

列索引：columns

pandas封装了matplotlib和numpy；读取数据方便；数据处理能力强。	 

DateFrame：添加了index和columns的ndarray。其中value就是ndarray。

修改行、列索引只能整体修改，不能单独修改某一个索引

重设索引：可以设置删除原来的索引（true），也可以选择保留原索引（默认，false），原索引成为一列数据

Multilndex：多级或多层索引对象，可以表示三维的数据

逻辑运算：

- query（布尔结果，比较字符串）
- isin（[值列表]）：判断是否是值列表中的值

统计函数：与numpy中的统计功能相同

累计统计：统计前n个数的指标

![image-20200917101507689](img/image-20200917101507689.png)

自定义运算：apply

~~~python
#例如：定义最大值-最小值
data.apply(lambda x : x.max() - x.min())	#使用lambda的简略写法
~~~



# pandas绘图

~~~python
pandas.DataFrame.plot(x=某列，y=某列，kind=“图类型”)
pandas.Series.plot
~~~



## 读取文件

文件类型：csv、hdf5、json



## 总结

![Pandas高级数据处理](img/Pandas%E9%AB%98%E7%BA%A7%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86.jpg)