# numpy

Numpy 高效的运算工具
Numpy的优势
ndarray属性
基本操作
    ndarray.方法()
    numpy.函数名()
ndarray运算
    逻辑运算
    统计运算
    数组间运算
合并、分割、IO操作、数据处理

1. ## numpy的优势

   ndarrary与python原生list对比

   1. 储存风格
      - ndarray - 相同类型 - 通用性不强
      - list - 不同类型 - 通用性很强
   2. 并行化运算：ndarray支持向量化运算
   3. 底层语言，ndarray，C语言，解除了GIL

2. ## numpy属性

ndarray.shape：几行，几列

ndarray.ndim：维度，秩

ndarray.size：数组长度（元素个数）

ndarray.dtype：元素类型

- 创建时没有指定元素，默认
  - 整形：int64
  - 浮点型：float64

ndarray.itemsize：每个元素的大小

3. ## 基本操作

ndarray.方法（）

np.函数（）

### 生成数组的方法

   1. 生成0和1

      - np.zeros(shape=)
      - np.ones(shape=)

2. 从现有数组中生成

   - np.array()、 np.copy()：深拷贝：新的数组元素是独立的，不随原数组变化而变化
   - np.asarray()：浅拷贝：新的数组随原数组变化而变化（相当于创建快捷方式）

3. 生成固定范围的数组

   - np.linspace(a, b, c)
     - 左闭右闭区间
     - 等距离c个元素
   - np.arange(a, b, c)
     - 左闭右开区间
     - 步长为c的n个元素
     - 不够步长的就舍弃掉，不显示
     - ![image-20200916105558450](img/image-20200916105558450.png)

4. 生成随机数组

   - 均匀分布：每组的可能性相等

     - ~~~python
       #随机均匀分布
       data1 = np.random.uniform(low=-1, high=1, size=1000000)
       ~~~

     - ![image-20200916110127178](img/image-20200916110127178.png)

   - 正态分布：σ 幅度、波动程度、集中程度、稳定性、离散程度

     - ~~~python
       # 正态分布
       data2 = np.random.normal(loc=1.75, scale=0.1, size=1000000)
       ~~~

     - ![image-20200916110221867](img/image-20200916110221867.png)

### 数组的索引、切片

ndarray 数组可以基于 0 - n 的下标进行索引，对象可以通内置的 slice 函数，并设置 start, stop 及 step 参数进行，从原数组中切割出一个新数组。

~~~python
import numpy as np

a = np.arange(10)
s = slice(2,7,2)   # 从索引 2 开始到索引 7 停止，间隔为2
print (a[s])	# [2  4  6]

#切片操作
a = np.arange(10)  
b = a[2:7:2]   # 从索引 2 开始到索引 7 停止，间隔为 2
print(b)	#[2  4  6]
~~~

冒号的使用：

- [num]：返回索引为num的元素
- [num:]：num到之后所有的元素
- [num:num1]：num到num1之间所有的元素
- 多维数组适用

对于二维数组

~~~python
stock_change[0:2, 0:5]
#代表前两个一维数组的前五个元素
~~~



### 形状修改

ndarray.reshape(shape)： 返回新的ndarray，原始数据没有改变

ndarray.resize(shape)： 没有返回值，对原始的ndarray进行了修改

ndarray.T：转置：行变成列，列变成行

### 类型修改

ndarray.astype(要修改为的类型)

ndarray序列化到本地，先转换为字节类型：ndarray.tobytes()

### 数组的去重

set方法只能对一维数据去重。多维数组.flatten()方法，得到一维的数组

np.unique（数组）：对数组去重，得到一维数组



4. ## ndarray运算

### 逻辑运算

向量化运算，整体数组比较

~~~python
某个数组 > num
#元素大于num则为true，反之则为false
~~~

#### 通用判断函数

- np.all(布尔值)：只要有一个False就返回False，只有全是True才返回True
- np.any(布尔值)：只要有一个True就返回True，只有全是False才返回False

#### np.where(三元运算符)

np.where(运算式，如果为true赋的值，如果为false赋的值)



### 统计运算

#### 统计指标函数

~~~python
#最小值，最大值，平均值，中位数，方差，标准差
min, max, mean, median, var, std
np.函数名
ndarray.方法名

#查询二维数组temp中第一个数组中的最大值
temp.max(axis=0)
~~~



#### 返回最大值、最小值所在位置

~~~python
np.argmax(temp, axis=)
np.argmin(temp, axis=)
#axis代表多维数组，要查询的数组的索引
~~~



### 数组间运算

数组与数的运算：向量化运算

数组与数组的运算：广播机制

~~~python
#此种情况是不能进行运算的
arr1 = np.array([1,1,1])
arr2 = np.array([1,1])
~~~

如果两数组：维度同/不同，后缘维度的轴长相符，可以进行运算

![image](img/890640-20180510210455543-2125343324.png)

![image](img/890640-20180510210457361-2084634558.png)

### 矩阵（matrix）运算

矩阵与array的区别：矩阵必须是二维的

存储矩阵的方法

- ndarray 二维数组

  - np.matmul
  - np.dot

  存储之后是 matrix数据结构

矩阵乘法

​	形状要求：（m，n）*（n，l）=（m，l）

​	前者的列必须与后者的行数相同

矩阵运算规则

![image-20200916145631105](img/image-20200916145631105.png)

5. ## 合并、分割

np.hstack（a，b）：水平拼接a和b

~~~python
a = np.array([1,2,3,4])
b = np.array([5,6,7,8])
np.hstack((a, b))	#array([1,2,3,4,5,6,7,8])
~~~



np.vstack（a，b）：竖直拼接

~~~python
a = np.array([1,2,3,4])
b = np.array([5,6,7,8])
np.vstack((a, b))	#array([[1,2,3,4],[5,6,7,8]])
~~~



np.concatenate((a，b)，axis=<0，1>)：水平、竖直拼接

当axis=1时：水平拼接

当axis=0时：竖直拼接



6. IO流操作

读取cvs文件（通常不用numpy读取数据）：

~~~python
np.genfromtxt("test.csv", delimiter=",")
#参数二：以逗号为分隔符
~~~

# 总结

![Numpy总结](numpy/img/Numpy%E6%80%BB%E7%BB%93.jpg)