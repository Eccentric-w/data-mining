# matplotlib

使用需要先导包：import matplotlib.pyplot as plt

## 基础绘制功能

### 画图的一般步骤

- 创建画布

  ```python
  plt.figure()
  
  #属性设置
  plt.figure(figsize=(),dpi=)
  figsize:设置画布的大小，长宽
  dpi：清晰度，每英寸显示的像素数量
  
  #保存图片
  plt.savefig(路径+文件名)
  ```

- 绘制图像

  ```python
  plt.plot(x轴数据，y轴数据)
  ```

- 显示图像

  ```python
  plt.show()
  ```

  plt.show() 会释放figure资源，如果在之后保存只能保存空白图片，所以应该先保存，在显示图片

## 三层结构

1. 容器层

   1. 画板层

      在最底层，通常不接触

   2. 画布层

      一个画布层可以有多个绘图区（figure）

   3. 绘图区/坐标系（figure）

      x、y轴

      坐标系（Axes）坐标轴（Axis）

2. 辅助显示层

   网格，刻度等，修饰图表

   

   修改x、y轴刻度

   ```python
   #修改x轴刻度，区间左闭右开，步长是5
   plt.xticks(rang(0,40,5))
   #修改y轴刻度，把数组按照步长为5分割
   plt.yticks(数组[::5])
   #修改x轴的刻度说明
   x_lable=["11点{}分".format(i) for i in x]	#字符串匹配，字符串格式化
   plt.xticks(x[::5],xlable[::5])	#步长为5分隔，每个刻度对应的说明是字符串
   ```

   网格（grid）类型

   ```python
   plt.grid(boolean，linestyle="",alpha=)
   #参数一：是否添加网格，默认是true
   #参数二：网格线类型，虚线“--”
   #参数三：透明度：0~1
   ```

   x、y轴标题信息

   ```python
   plt.xlabel(x轴标题)
   plt.ylabel(y轴标题)
   plt.title(图标题)
   ```

   代码演示

   ```python
   # 需求：画出某城市11点到12点1小时内每分钟的温度变化折线图，温度范围在15度~18度
   import random
   
   # 1、准备数据 x y
   x = range(60)
   y_shanghai = [random.uniform(15, 18) for i in x]	#15到18直接随机
   
   # 2、创建画布
   plt.figure(figsize=(20, 8), dpi=80)
   
   # 3、绘制图像
   plt.plot(x, y_shanghai)
   
   # 修改x、y刻度
   # 准备x的刻度说明
   x_label = ["11点{}分".format(i) for i in x]
   plt.xticks(x[::5], x_label[::5])
   plt.yticks(range(0, 40, 5))
   
   # 添加网格显示
   plt.grid(linestyle="--", alpha=0.5)
   
   # 添加描述信息
   plt.xlabel("时间变化")
   plt.ylabel("温度变化")
   plt.title("某城市11点到12点每分钟的温度变化状况")
   
   # 4、显示图
   plt.show()
   ```

   

3. 图像层

在绘图区画图



添加多条数据对象：

```python
#在同一图中，画多条数据
plt.plot()	#有几个数据，就plot几次
#参数color，改图线颜色
#参数linestyle，线条风格
```



在一个画布上，创建多个图

~~~python
figure, axes = plt.subplots(nrows=1, ncols=2, figsize=(20, 8), dpi=80)
#返回值：画布对象和图列表（axes[]）
#参数一：行数
#参数二：列数
#参数三：画布大小长宽
#参数四：清晰度，每英寸像素点
# 对图像操作时，通过axes[num]

~~~

对多个表修饰时，要用面向对象的方式
plt.函数名：面向过程

#### axes.set_方法名：面向过程

- set_xticks：x轴刻度
- set_yticks：y轴刻度
- set_xlabel：x轴标题
- set_ylabel：y轴标题
- axes.grid（）：添加网格显示



多种图形绘制

~~~python
#散点图
plt.scatter(x轴data,y轴data)

#柱形图
plt.bar（x、y轴数据，<with=宽度>,<柱的颜色列表>）

#直方图
#组数公式：组数=数据列表极差/组距（x轴刻度间距）
plt.hist(数据列表，bins=组数，<density=是否将y轴转换为频率，默认是flase>)
#组距影响观看，可以多次尝试改变组距

#饼图
plt.pie(数据列表，<labels=标题列表>，<colors=颜色列表>，autopct="显示百分占比数字格式")
~~~





## 总结

![](img/image-20200916102006492.png)