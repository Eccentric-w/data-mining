# pandas数据处理



## NaN缺失值处理

 

处理方法：删除缺失值；替换/插补缺失值。

判断是否存在NaN：

- pd.isnull(df)：在df中，缺失值的地方返回true，不是的地方返回值false
- pd.notnull （df）：与isnull方法的返回值相反

删除缺失值：

df.dropna（inplace=）：删除存在缺失值的行

参数：为true：执行在原数据中删除；为false（默认）：返回一个删除了缺失值的新df

替换/填补：

df.fillna(value，inplace=)：填补缺失值

参数：value：要填充的值；inplace：用法同上



## 特殊标记值处理

替换：标记值-> np.nan

df.replace(to_replace=标记值，value=np.nan)

之后按照处理nan的方式处理



## 数据离散化

避免由于数据的差异影响数值的平衡；

可以简化数据结构；可以用来减少给定连续属性值的个数。

one-hot编码&哑变量

###  实现离散化

分组：返回Series对象

- 自动分组：pd.qcut(需要分组的数据，要分成的组数)
- 自定义分组：pf.cut（需要分组的数据，[分组的区间，以列表形式传入]）

分组好的结果转换成one-hot编码：

pd.get_dummies（分好组的Series，prefix=分好组的组名前缀）



## 合并

 按方向合并： 

- pd.concat((数据列表)，axis=<0,1>)，0：竖直合并，1：水平合并

按索引合并：

pd.merge（左表，右表，how=连接方式（默认内连接），on=[连接字段]）

连接方式（空值用nan表示）：

- inner：内连接，合并连接字段中共有的键
- left：左连接，左表中的连接字段都保留，右表以左表为准，左表没有的字段右表不保留
- right：右连接，以右表为准
- outer：外连接，左右表的连接字段都保留



## 交叉表和透视表



### 交叉表crosstab

寻找两列之间的关系：一列数据对另一列数据的分组个数

~~~python
pd.crosstab(value1,value2)
~~~



## 总结

![pandas数据处理](pandas%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86/img/pandas%E6%95%B0%E6%8D%AE%E5%A4%84%E7%90%86.jpg)